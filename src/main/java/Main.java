import controller.Controller;
import view.View;

public class Main {

    public static void main(String[] args) {
        Controller c = new Controller(View.Type.PLAIN);

//        Controller c = new Controller(View.Type.JSON);


        Thread[] threads = c.start(200, 20);

//        for (Thread t : threads) {
//            t.interrupt();
//        }

    }
}
