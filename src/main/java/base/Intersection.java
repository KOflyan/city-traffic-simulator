package base;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@EqualsAndHashCode
@ToString
public abstract class Intersection {

    @Getter
    private String name;
    @Getter @Setter
    private IntersectionType type = IntersectionType.DEFAULT;

    public enum IntersectionType {
        DEFAULT, CITY_ENTRANCE, CAR_SERVICE
    }

    public Intersection(String name) {
        this.name = name;
    }
}
