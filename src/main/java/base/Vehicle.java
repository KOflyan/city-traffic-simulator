package base;

import environment.EnvironmentService;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import network.City;
import network.Street;

import java.util.List;
import java.util.Random;
import java.util.UUID;

@EqualsAndHashCode(exclude = {"r", "city", "service", "lastIntersection", "currentStreet", "target"})
@ToString(exclude = {"r", "city", "service", "lastIntersection", "currentStreet", "target"})
public abstract class Vehicle implements Runnable {

    protected final Random r = new Random();

    @Getter protected String id = UUID.randomUUID().toString();
    @Getter protected City city;
    @Getter protected EnvironmentService service;
    @Getter @Setter protected EngineType engineType;
    @Getter @Setter protected State state = State.NORMAL;
    @Getter protected Intersection lastIntersection;
    @Getter protected Vehicle target;
    @Getter protected Street currentStreet;

    public enum EngineType {
        DIESEL, GASOLINE, LEMONADE, ELECTRIC
    }

    public enum State {
        NORMAL, BAD, CANNOT_MOVE, PERFECT
    }

    protected Vehicle(EngineType engineType, EnvironmentService service, City city, Street currentStreet) {
        this.engineType = engineType;
        this.service = service;
        this.city = city;
        this.currentStreet = currentStreet;
    }

    protected Vehicle(EnvironmentService service, City city, Vehicle target) {
        this.service = service;
        this.city = city;
        this.target = target;
    }

    protected Street getNextStreet() {
        List<Street> streets = city.getMap().get(lastIntersection);
        return streets.get(r.nextInt(streets.size()));
    }

    public abstract void drive() throws InterruptedException;
}
