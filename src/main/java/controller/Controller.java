package controller;

import vehicles.Car;
import environment.EnvironmentService;
import lombok.Getter;
import lombok.Setter;
import network.*;
import view.View;

import java.util.Random;

public class Controller {

    private final Random r = new Random();

    @Getter @Setter
    private EnvironmentService service;
    @Getter @Setter
    private City city;
    @Getter @Setter
    private View view;


    public Controller(View.Type type) {
        service = new EnvironmentService();
        city = new City();
        view = new View();
        showInformation(type);
    }

    private void showInformation(View.Type type) {
        service.showInformation(view, city, type);
    }


    private Street[] initializeCity() {
        Street[] cityEntrances = {

                new Street("Pineapple st",
                        new IntersectionCityEntrance("Pineapple-Town"),
                        new IntersectionDefault("Pineapple-Blueberry"),
                        new IntersectionDefault("Lovely-Pineapple-Pear"),
                        new IntersectionDefault("Pineapple-Ruby")
                ),

                new Street("Apple st",
                        new IntersectionCityEntrance("Apple-Town"),
                        new IntersectionDefault("Apple-Blueberry"),
                        new IntersectionCarService("Apple-Pear"),
                        new IntersectionCarService("Apple-Rose"),
                        new IntersectionDefault("Apple-Orange")
                ),

                new Street("Carrot st", Street.StreetState.BAD,
                        new IntersectionCityEntrance("Carrot-Town"),
                        new IntersectionDefault("Cozy-Carrot"),
                        new IntersectionDefault("Dragon-Carrot")
                ),

                new Street("Raspberry st",
                        new IntersectionCityEntrance("Raspberry-Town"),
                        new IntersectionCarService("Raspberry-Wizard-Cozy"),
                        new IntersectionDefault("Cherry-Dragon-Raspberry")
                )
        };

        Street[] streets = {

                cityEntrances[0],

                cityEntrances[1],

                cityEntrances[2],

                cityEntrances[3],

                new Street("Rose st",
                        new IntersectionCarService("Apple-Rose"),
                        new IntersectionDefault("Orange-Rose")
                ),

                new Street("Orange st",
                        new IntersectionDefault("Apple-Orange"),
                        new IntersectionDefault("Orange-Rose"),
                        new IntersectionDefault("Orange-Wizard")
                ),

                new Street("Wizard st",
                        new IntersectionDefault("Orange-Wizard"),
                        new IntersectionDefault("Grapes-Wizard-Pear"),
                        new IntersectionDefault("Wizard-Blueberry"),
                        new IntersectionCarService("Raspberry-Wizard-Cozy")
                ),

                new Street("Cozy st",
                        new IntersectionCarService("Raspberry-Wizard-Cozy"),
                        new IntersectionDefault("Cozy-Carrot")
                ),


                new Street("Blueberry st",
                        new IntersectionDefault("Pineapple-Blueberry"),
                        new IntersectionDefault("Apple-Blueberry"),
                        new IntersectionDefault("Wizard-Blueberry"),
                        new IntersectionDefault("Dragon-Blueberry")
                ),

                new Street("Dragon st",
                        new IntersectionDefault("Dragon-Carrot"),
                        new IntersectionDefault("Cherry-Dragon-Raspberry"),
                        new IntersectionDefault("Dragon-Blueberry"),
                        new IntersectionDefault("Grapes-Dragon")
                ),

                new Street("Pear st",
                        new IntersectionDefault("Lovely-Pineapple-Pear"),
                        new IntersectionCarService("Apple-Pear"),
                        new IntersectionDefault("Grapes-Wizard-Pear")
                ),

                new Street("Ruby st", Street.StreetState.BAD, new IntersectionDefault("Pineapple-Ruby")),

                new Street("Lovely st",
                        new IntersectionDefault("Lovely-Pineapple-Pear"),
                        new IntersectionCarService("Lovely-Cold-Rocky")
                ),

                new Street("Rocky st", new IntersectionCarService("Lovely-Cold-Rocky")),

                new Street("Cold st",
                        new IntersectionCarService("Lovely-Cold-Rocky"),
                        new IntersectionDefault("Cold-Sad-Lucky")
                ),

                new Street("Sad st", new IntersectionDefault("Cold-Sad-Lucky")),

                new Street("Lucky st", new IntersectionDefault("Cold-Sad-Lucky")),

                new Street("Grapes st",
                        new IntersectionDefault("Grapes-Dragon"),
                        new IntersectionDefault("Grapes-Wizard-Pear"),
                        new IntersectionDefault("Grapes-Melon")

                ),

                new Street("Melon st",
                        new IntersectionDefault("Grapes-Melon"),
                        new IntersectionDefault("Cherry-Melon")
                ),

                new Street("Cherry st",
                        new IntersectionDefault("Cherry-Melon"),
                        new IntersectionDefault("Cherry-Dragon-Raspberry")
                )

        };

        city.addStreets(streets);

        return cityEntrances;
    }

    public Thread[] start(int totalCars, int environmentFriendlyCars) {
        Street[] cityEntrances = initializeCity();
        Thread[] t = new Thread[totalCars];

        for (int i = 0; i < environmentFriendlyCars; i++) {
            Car.EngineType engine = r.nextInt(2) == 0 ? Car.EngineType.ELECTRIC : Car.EngineType.LEMONADE;
            Street start = cityEntrances[r.nextInt(cityEntrances.length)];
            t[i] = new Thread(new Car(engine, service, city, start));
            t[i].start();
        }

        for (int i = environmentFriendlyCars; i < totalCars; i++) {
            Car.EngineType engine = r.nextInt(2) == 0 ? Car.EngineType.DIESEL : Car.EngineType.GASOLINE;
            Street start = cityEntrances[r.nextInt(cityEntrances.length)];
            t[i] = new Thread(new Car(engine, service, city, start));
            t[i].start();
        }

        return t;
    }

}