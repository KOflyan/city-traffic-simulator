package environment;

import base.Vehicle;
import bird.Bird;
import vehicles.Car;
import vehicles.RepairingVehicle;
import lombok.Getter;
import network.City;
import network.IntersectionCarService;
import view.View;

import java.util.*;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.logging.Logger;

public class EnvironmentService {

    private final Logger logger = Logger.getLogger(EnvironmentService.class.getName());
    private final Random r = new Random();
    private final Timer timer = new Timer();

    @Getter private final Set<Vehicle> cars = new HashSet<>();
    @Getter private final Set<Vehicle> repairingVehicles = new HashSet<>();
    @Getter private double totalPollutionRate = 0;

    public EnvironmentService() {

        timer.schedule(new TimerTask() {
            @Override
            public void run() {
                Bird.sing(totalPollutionRate);
            }
        }, 5000, 4000);
    }

    public void showInformation(View view, City city, View.Type type) {

        timer.schedule(new TimerTask() {
            @Override
            public void run() {
                view.showTransportInformation(cars, repairingVehicles, totalPollutionRate, city, type);

            }
        }, 10000, 5000);
    }

    public void sendInformation(Car car) {
        totalPollutionRate += car.getPollutionRate();
    }

    public void registerCar(Vehicle car) {
        synchronized (cars) {
            cars.add(car);
        }
    }

    private void registerRepairingVehicle(Vehicle vehicle) {
        synchronized (repairingVehicles) {
            repairingVehicles.add(vehicle);
            new Thread(vehicle).start();
        }
    }

    public void removeRepairingVehicle(Vehicle vehicle) {
        synchronized (repairingVehicles) {
            if (repairingVehicles.contains(vehicle)) {
                repairingVehicles.remove(vehicle);
            }
        }
    }

    public void sendRepairingVehicle(Vehicle target) {
        registerRepairingVehicle(new RepairingVehicle(this, target.getCity(), target));
    }

    public void repairCar(Function<Car, String> func, Car car, IntersectionCarService is) throws InterruptedException {

        synchronized (cars) {

            while (is.isInRepairState()) {
                cars.wait();
            }

            is.setInRepairState(true);

            Thread.sleep(500);

            if (car.isEngineToChange()) {
                car.setEngineType(r.nextInt(2) == 0 ? Car.EngineType.ELECTRIC : Car.EngineType.LEMONADE);
                car.setEngineToChange(false);
            }

            logger.info(String.format("Repairing the vehicles ( %s ), please stand by . . .\n", func.apply(car)));

            is.setInRepairState(false);

            cars.notifyAll();
        }
    }


    public boolean isDrivingAllowed(Vehicle vehicle) {
        //System.out.println(totalPollutionRate);

        switch (vehicle.getEngineType()) {

            case DIESEL:
                return totalPollutionRate < 400;

            case GASOLINE:
                return totalPollutionRate > 500;

            default:
                return true;
        }
    }

    public void update() {

        synchronized (cars) {
            Predicate<Vehicle> predicate = car ->
                    car.getEngineType() == Car.EngineType.GASOLINE || car.getEngineType() == Car.EngineType.DIESEL;

            long carsWithInternalCombustionEngines = cars.parallelStream()
                    .filter(predicate)
                    .count();

            if (carsWithInternalCombustionEngines < 70) {
                totalPollutionRate = 0;
            } else {
                totalPollutionRate = totalPollutionRate * 0.4;
            }

//            cars.forEach(Object::notify);
        }
    }
}
