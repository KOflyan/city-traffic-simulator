package network;

import base.Intersection;
import lombok.Getter;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Used source: https://gist.github.com/smddzcy/bf8fc17dedf4d40b0a873fc44f855a58#file-solution-java
 */

public class City {

    @Getter private final List<Street> streets;
    @Getter private final Map<Intersection, List<Street>> map;

    public City() {
        streets = new ArrayList<>();
        map = new HashMap<>();
    }

    public void addStreet(Street s) {
        if (streets.contains(s)) {
            return;
        }
        streets.add(s);
        for (Intersection i : s.getIntersections()) {
            if (!map.containsKey(i)) {
                map.put(i, new ArrayList<>());
            }
            map.get(i).add(s);
        }
    }

    public void addStreets(Street[] streets) {
        for (Street s : streets) {
            addStreet(s);
        }
    }
}