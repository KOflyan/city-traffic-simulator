package network;

import base.Intersection;
import lombok.Getter;
import lombok.Setter;

public class IntersectionCarService extends Intersection {

    @Getter @Setter
    private boolean isInRepairState = false;

    public IntersectionCarService(String name) {
        super(name);
        super.setType(IntersectionType.CAR_SERVICE);
    }


}
