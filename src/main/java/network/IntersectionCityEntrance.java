package network;

import base.Intersection;

public class IntersectionCityEntrance extends Intersection {

    public IntersectionCityEntrance(String name) {
        super(name);
        super.setType(IntersectionType.CITY_ENTRANCE);
    }
}
