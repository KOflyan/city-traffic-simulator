package network;

import base.Intersection;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.ToString;

import static network.Street.StreetState.NORMAL;

@EqualsAndHashCode
@ToString(exclude = "intersections")
public class Street {

    @Getter private String name;
    @Getter private Intersection[] intersections;
    @Getter private StreetState state = NORMAL;

    public enum StreetState {
        NORMAL, BAD
    }

    public Street(String name, Intersection... intersections) {
        this.name = name;
        this.intersections = intersections;
    }

    public Street(String name, StreetState state, Intersection... intersections) {
        this.name = name;
        this.intersections = intersections;
        this.state = state;
    }
}
