package vehicles;

import base.Intersection;
import base.Vehicle;
import environment.EnvironmentService;
import lombok.Getter;
import lombok.Setter;
import network.City;
import network.IntersectionCarService;
import network.Street;

public class Car extends Vehicle {

    @Getter @Setter private boolean engineToChange = false;

    private long streetsDriven = 0;
    private int timesStopped = 0;

    public Car(EngineType engineType, EnvironmentService service, City city, Street currentStreet) {
        super(engineType, service, city, currentStreet);
        service.registerCar(this);
    }

    public double getPollutionRate() throws IllegalArgumentException {

        switch (engineType) {

            case DIESEL:
                return 3;

            case GASOLINE:
                return 2;

            case LEMONADE:
                return 0.5;

            case ELECTRIC:
                return 0.1;

            default:
                throw new IllegalArgumentException(String.format("Incorrect engine type ( %s ).", engineType));
        }
    }

    public void drive() throws InterruptedException {

        // Choose random intersection
        Intersection[] intersections = currentStreet.getIntersections();
        lastIntersection = intersections[r.nextInt(intersections.length)];

        if (lastIntersection.getType() == Intersection.IntersectionType.CAR_SERVICE) {

            service.repairCar(Car::toString, this, (IntersectionCarService) lastIntersection);
        }

        currentStreet = getNextStreet();

        checkCarState();

        ++streetsDriven;
    }

    private void checkCarState() {
        if (currentStreet.getState() == Street.StreetState.BAD) {
            switch (state) {
                case NORMAL:
                    state = State.BAD;
                    break;
                case BAD:
                    state = State.CANNOT_MOVE;
                    break;
            }
        }

        if (state == State.CANNOT_MOVE) {
            //System.out.println("BROKEN");
            service.sendRepairingVehicle(this);
        }
    }


    @Override
    public void run() {

        boolean isDrivingAllowed;

        while (!Thread.interrupted()) {

            if (state == State.CANNOT_MOVE) {
                continue;
            }

            isDrivingAllowed = true;

            try {

                drive();

                if (streetsDriven % 5 == 0) {
                    service.sendInformation(this);
                }

                if (streetsDriven % 7 == 0) {
                    isDrivingAllowed = service.isDrivingAllowed(this);
                }

                if (!isDrivingAllowed) {
                    ++timesStopped;
                    Thread.sleep(2000);
                    if (timesStopped >= 2 && r.nextInt(101) > ((double) 1 / 6 * 100)) {
                        engineToChange = true;
                    }

                    service.update();
                }

            } catch (InterruptedException ignored) {
            }
        }
    }
}
