package vehicles;

import base.Intersection;
import base.Vehicle;
import environment.EnvironmentService;
import network.City;
import network.Street;

import java.util.List;
import java.util.logging.Logger;

public class RepairingVehicle extends Vehicle {

//    private final Logger logger = Logger.getLogger(RepairingVehicle.class.getName());

    public RepairingVehicle(EnvironmentService service, City city, Vehicle target) {
        super(service, city, target);

        List<Street> streets = city.getStreets();
        currentStreet = streets.get(r.nextInt(streets.size()));
        engineType = EngineType.GASOLINE;
    }

    @Override
    public void drive() {
        Intersection[] intersections = currentStreet.getIntersections();
        lastIntersection = intersections[r.nextInt(intersections.length)];
        currentStreet = getNextStreet();
    }

    private void repair() {


        if (target.getEngineType() == EngineType.LEMONADE || target.getEngineType() == EngineType.ELECTRIC) {
            target.setState(State.PERFECT);
        } else {
            target.setState(State.NORMAL);
        }

        try {
            Thread.sleep(50);
        } catch (InterruptedException ignored) {}
    }

    private void flush() {
        service.removeRepairingVehicle(this);
    }

    @Override
    public void run() {

        while (!currentStreet.equals(target.getCurrentStreet())) {
            drive();
        }

        repair();

        //flush();
    }
}
