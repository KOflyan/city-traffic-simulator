package view;

import base.Intersection;
import base.Vehicle;
import lombok.Getter;
import network.City;
import network.Street;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.time.LocalDateTime;
import java.util.Collections;
import java.util.Set;

public class View {

    @Getter
    private final String FILE_PATH = "src/resources/";
    @Getter
    private final String FILE_NAME = "info.txt";

    private final String DELIMITER_1 = "=";
    private final String DELIMITER_2 = "-";
    private final String DELIMITER_3 = "~";

    private String fileNameWithPath;
    private BufferedWriter w;

    public View() {
        fileNameWithPath = FILE_PATH + FILE_NAME;
    }

    public enum Type {
        PLAIN, JSON
    }

    private void writeDelimiter(String delimiter, int length) {
        try {
            w.write(String.join("", Collections.nCopies(length, delimiter)));
            w.write("\n");

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void write(String message) {
        try {
            w.write(message + "\n");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void dumpCommon(Set<Vehicle> cars, int streets, int repairingVehicles, int intersections, double pollutionRate) {
        writeDelimiter(DELIMITER_3, 80);
        write("\n");

        String d = LocalDateTime.now().toString();
        writeDelimiter(DELIMITER_2, d.length());
        write(d);
        writeDelimiter(DELIMITER_2, d.length());
        write("\n");

        write("GENERAL INFORMATION");
        writeDelimiter(DELIMITER_1, 50);


        write("\tTotal vehicles: " + (cars.size() + repairingVehicles));
        write("\t\tCars: " + cars.size());
        long carsWithInternalCombustionEngine = getCarsWithInternalCombustionEngine(cars);
        write("\t\t\tWith internal combustion engines: " + carsWithInternalCombustionEngine);
        write("\t\t\tWith other engines: " + (cars.size() - carsWithInternalCombustionEngine));
        write("\t\tRepairing Vehicles Sent: " + repairingVehicles);
        write("\tStreets: " + streets);
        write("\tIntersections: " + intersections);
        write("\n");
        write("Pollution rate: " + pollutionRate + "\n");
        writeDelimiter(DELIMITER_1, 50);
        write("\n\n");
    }

    private long getCarsWithInternalCombustionEngine(Set<Vehicle> cars) {
        return cars.stream()
                .filter(c -> c.getEngineType() == Vehicle.EngineType.GASOLINE || c.getEngineType() == Vehicle.EngineType.DIESEL)
                .count();
    }

    private void dumpDetailed(Set<Vehicle> cars, Set<Vehicle> repair, City city) {
        write("DETAILED INFORMATION");
        writeDelimiter(DELIMITER_1, 80);
        write("\n");
        write("VEHICLES - CARS");
        writeDelimiter(DELIMITER_2, 30);
        write("\n");
        for (Vehicle c : cars) {
            write("\t" + c);
        }
        write("\n");
        writeDelimiter(DELIMITER_2, 30);
        write("\n");
        write("VEHICLES - REPAIR");
        writeDelimiter(DELIMITER_2, 30);
        write("\n");
        for (Vehicle c : repair) {
            write("\t" + c);
        }
        write("\n");
        writeDelimiter(DELIMITER_2, 30);
        write("\n");
        write("STREETS && INTERSECTIONS");
        writeDelimiter(DELIMITER_2, 30);
        write("\n");
        for (Street s : city.getStreets()) {
            write("\t" + s);
            for (Intersection i : s.getIntersections()) {
                write("\t\t" + i);
            }
            write("\n");
        }
        write("\n");
        writeDelimiter(DELIMITER_2, 30);

        write("\n");
        writeDelimiter(DELIMITER_3, 80);
    }

    public void showTransportInformation(Set<Vehicle> cars, Set<Vehicle> repairingVehicles,
                                         double pollutionRate, City city, Type  type) {
        open();
        if (type == Type.PLAIN) {
            dumpCommon(cars, city.getStreets().size(), repairingVehicles.size(), city.getMap().keySet().size(), pollutionRate);
            dumpDetailed(cars, repairingVehicles, city);
        } else {
            long carsWithInternalCombustionEngine = getCarsWithInternalCombustionEngine(cars);
            String json = String.format("{\n" +
                    "  \"timestamp\": %s,\n" +
                    "  \"vehicles\": {\n" +
                    "    \"cars\": {\n" +
                    "      \"With internal combustion engine\": %s,\n" +
                    "      \"With other engine\": %s,\n" +
                    "      \"total:\": %s\n" +
                    "    }, \n" +
                    "    \"repairing vehicles sent\": %s,\n" +
                    "    \"total\": %s\n" +
                    "  },\n" +
                    "  \n" +
                    "  \"city\": {\n" +
                    "    \"Streets\": %s,\n" +
                    "    \"Intersections\": %s\n" +
                    "  }\n" +
                    "}", LocalDateTime.now(), carsWithInternalCombustionEngine,
                    cars.size() - carsWithInternalCombustionEngine, cars.size(), repairingVehicles.size(),
                    repairingVehicles.size() + cars.size(), city.getStreets().size(), city.getMap().keySet().size());
            write(json);
        }
        close();
    }

    private void open() {
        try {
            w = new BufferedWriter(new FileWriter(new File(fileNameWithPath)));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void close() {
        try {
            w.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
